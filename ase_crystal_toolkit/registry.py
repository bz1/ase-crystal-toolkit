"""
This module registers the view provided in this package
"""
from ase.utils.plugins import ExternalViewer
VIEWER_ENTRYPOINT = ExternalViewer(
    desc="Visualization using crystal-toolkit",
    module="ase_crystal_toolkit.viewer"
)

